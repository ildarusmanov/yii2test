# Установка 

## Установка для сервера nodejs

```
npm install ws
npm install request
npm install url

```
## Устновка Yii2

Перед началом работы необходимо настроить подключение к MySQL в файле config/db.php


Далее

```
composer install

php yii migrate/up --migrationPath=@vendor/dektrium/yii2-user/migrations

php yii migrate

php yii notifications/init
```

# Запуск

## Yii2
```
cd {project_dir}/web
php -S 0.0.0.0:5000
```

## NodeJS
```
cd {project_dir}
nodejs ws.js
```

Сервер доступен по адресу http://0.0.0.0:5000

# Доступ

Зарегистрируйте аккаунт под логином admin

# Добавление способа отправки уведомлений

Создать класс провайдера для нужного способа отправки, который наследуется от класса 
```
app\services\notifications\senders\SenderAbstract
```
метод canSend() определяет возможость оправки уведомления данным способом
метод send($message) осуществляет отправку уведомления с текстом $message

После создания провайдера для отправки уведомлений необходимо его доьавить через веб-интерфейс в разделе Notifications - Providers.

# Дополниттельно

## Затраченное время

~20 часов

##Что не успелось/осталось сделать: 

* 1) покрыть тестами
* 2) поправить дизайн, сделать вывод в таблицах более симпатичным
* 3) сделать аутентификацию в nodejs
* 4) i18n
* 5) очередь на Redis для нотификаций
* 6) документация
* 7) демо

## Контакты

E-mail: wajox@mail.ru

## Резюме

https://www.superjob.ru/resume/programmist-21517769.html
