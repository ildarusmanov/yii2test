<?php
namespace app\commands;

use yii\console\Controller;
use app\models\Eventid;
use app\models\NotificationSender as Sender;

class NotificationsController extends Controller
{

	public function actionInit()
	{
		$this->createSenders();
		$this->createEvents();
	}

	protected function createSenders()
	{
		$model = new Sender();
		$model->sender_class = '\app\services\notifications\senders\BrowserSender';
		$model->title = 'Browser';
		$model->save();

		$model = new Sender();
		$model->sender_class = '\app\services\notifications\senders\EmailSender';
		$model->title = 'Email';
		$model->save();
	}

	protected function createEvents()
	{
		$model = new Eventid();
		$model->event_class = '\app\models\User';
		$model->event_id = 'afterInsert';
		$model->title = 'New user registered';
		$model->save();

		$model = new Eventid();
		$model->event_class = '\app\models\News';
		$model->event_id = 'afterInsert';
		$model->title = 'New news added';
		$model->save();
	}
}