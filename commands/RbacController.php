<?php
namespace app\commands;

use yii\console\Controller;
use app\services\rbac\UserRoleRule;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = \Yii::$app->authManager;

        $rule = new UserRoleRule();
        $auth->add($rule);

        $manageNews = $auth->createPermission('manageNews');
        $manageNews->description = 'Create, Update, Delete news';
        $auth->add($manageNews);

        $manageNotifications = $auth->createPermission('manageNotifications');
        $manageNotifications->description = 'Create, Update, Delete Templates, Send Notifications';
        $auth->add($manageNotifications);

        $viewNews = $auth->createPermission('viewNews');
        $viewNews->description = 'View full news content';
        $auth->add($viewNews);

        $manageUsers = $auth->createPermission('manageUsers');
        $manageUsers->description = 'View, Create, Update, Delete users';
        $auth->add($manageUsers);

        $user = $auth->createRole('user');
        $user->description = 'User';
        $user->ruleName = $rule->name;
        $auth->add($user);
        $auth->addChild($user, $viewNews);

        $manager = $auth->createRole('manager');
        $manager->description = 'Manager/Moderator';
        $manager->ruleName = $rule->name;
        $auth->add($manager);
        $auth->addChild($manager, $manageNews);
        $auth->addChild($manager, $user);

        $admin = $auth->createRole('admin');
        $admin->description = 'Administrator';
        $admin->ruleName = $rule->name;
        $auth->add($admin);
        $auth->addChild($admin, $manageUsers);
        $auth->addChild($admin, $manageNotifications);
        $auth->addChild($admin, $manager);
    }
}
