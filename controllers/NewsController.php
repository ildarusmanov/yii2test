<?php

namespace app\controllers;

use Yii;
use app\models\News;
use app\services\news\NewsBuilder;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends Controller
{
    const DEFAULT_PAGE_LIMIT = 20;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view', 'create', 'update', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['?', '@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['view'],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'delete'],
                        'roles' => ['manager'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionIndex($limit = self::DEFAULT_PAGE_LIMIT)
    {
        $limit = intval($limit);

        $dataProvider = new ActiveDataProvider([
            'query' => News::find()->orderBy('[[created_at]] DESC'),
            'pagination' => [
                'pageSize' => $limit,
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'limit' => $limit,
        ]);
    }

    /**
     * Displays a single News model.
     * @param  integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new News();
        $request = \Yii::$app->request;
        $builder = $this->getBuilder($model);

        if ($request->isPost && $builder->save($request)) {
            return $this->redirect([
                'view',
                'id' => $builder->getModel()->id,
            ]);
        }

        return $this->render('create', [
            'model' => $builder->getModel(),
        ]);
    }

    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param  integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $request = \Yii::$app->request;
        $builder = $this->getBuilder($model);

        if ($request->isPost && $builder->save($request)) {
            return $this->redirect([
                'view',
                'id' => $builder->getModel()->id,
            ]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param  integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param  integer               $id
     * @return News                  the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function getBuilder($model = null)
    {
        $user = \Yii::$app->user->identity;

        return new NewsBuilder($user, $model);
    }
}
