<?php
namespace app\helpers;

use yii\helpers\ArrayHelper;
use app\models\User;
use app\models\Eventid;
use app\models\NotificationSender;

class FormHelper
{
    public static function getUserGroupsList()
    {
        return User::getRolesIdsList();
    }

    public static function getUsersList()
    {
        return ArrayHelper::map(User::find()->all(), 'id', 'email');
    }

    public static function getSendersList()
    {
    	return ArrayHelper::map(NotificationSender::find()->all(), 'id', 'title');
    }

    public static function getEventidsList()
    {
        return ArrayHelper::map(Eventid::find()->all(), 'id', 'title');
    }
}
