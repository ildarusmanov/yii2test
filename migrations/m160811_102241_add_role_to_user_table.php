<?php

use yii\db\Migration;

class m160811_102241_add_role_to_user_table extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'role_id', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'role_id');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
