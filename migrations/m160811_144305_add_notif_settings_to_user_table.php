<?php

use yii\db\Migration;

class m160811_144305_add_notif_settings_to_user_table extends Migration
{
    public function up()
    {
        $this->addColumn('{{%profile}}', 'notify_by_browser', $this->integer());
        $this->addColumn('{{%profile}}', 'notify_by_email', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('{{%profile}}', 'notify_by_browser');
        $this->dropColumn('{{%profile}}', 'notify_by_email');

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
