<?php
use yii\db\Migration;

/**
 * Handles the creation for table `event_notifications`.
 */
class m160812_104006_create_event_notifications_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%event_notification%}}', [
            'id' => $this->primaryKey(),
            'eventid_id' => $this->integer()->notNull(),
            'sender_id' => $this->integer()->notNull(),
            'user_role_id' => $this->integer()->notNull(),
            'title' => $this->string()->notNull(),
            'content' => $this->text()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%event_notification}}');
    }
}
