<?php

use yii\db\Migration;

/**
 * Handles the creation for table `eventid`.
 */
class m160813_154447_create_eventid_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%eventid}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'event_class' => $this->string()->notNull(),
            'event_id' => $this->string()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%eventid}}');
    }
}
