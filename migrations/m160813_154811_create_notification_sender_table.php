<?php

use yii\db\Migration;

/**
 * Handles the creation for table `notification_sender`.
 */
class m160813_154811_create_notification_sender_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%notification_sender}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'sender_class' => $this->string()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%notification_sender}}');
    }
}
