<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "event_notification".
 *
 * @property integer $id
 * @property integer $eventid_id
 * @property integer $sender_id
 * @property integer $user_role_id
 * @property string $title
 * @property string $content
 */
class EventNotification extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event_notification';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['eventid_id', 'sender_id', 'user_role_id', 'title', 'content'], 'required'],
            [['eventid_id', 'sender_id', 'user_role_id'], 'integer'],
            [['content'], 'string'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'eventid_id' => Yii::t('app', 'Eventid ID'),
            'sender_id' => Yii::t('app', 'Sender ID'),
            'user_role_id' => Yii::t('app', 'User Role ID'),
            'title' => Yii::t('app', 'Title'),
            'content' => Yii::t('app', 'Content'),
        ];
    }

    public function getSubscribedUsers()
    {
         return $this->hasMany(User::className(), ['role_id' => 'user_role_id']);
    }
}
