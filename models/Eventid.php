<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "eventid".
 *
 * @property integer $id
 * @property string $title
 * @property string $event_class
 * @property string $event_id
 */
class Eventid extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'eventid';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'event_class', 'event_id'], 'required'],
            [['title', 'event_class', 'event_id'], 'string', 'max' => 255],
            [['event_class'], 'unique', 'targetAttribute' => ['event_class', 'event_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'event_class' => Yii::t('app', 'Event Class'),
            'event_id' => Yii::t('app', 'Event ID'),
        ];
    }
}
