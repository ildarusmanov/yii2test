<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "notification_sender".
 *
 * @property integer $id
 * @property string $title
 * @property string $sender_class
 */
class NotificationSender extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notification_sender';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'sender_class'], 'required'],
            [['title', 'sender_class'], 'string', 'max' => 255],
            [['sender_class'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'sender_class' => Yii::t('app', 'Sender Class'),
        ];
    }
}
