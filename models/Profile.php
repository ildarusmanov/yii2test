<?php
namespace app\models;

use dektrium\user\models\Profile as BaseProfile;

class Profile extends BaseProfile
{

    const NOTIFY_BY_EMAIL_FIELD = 'notify_by_email';
    const NOTIFY_BY_BROWSER_FIELD = 'notify_by_browser';

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        // add field to scenarios
        $scenarios['create'][]   = self::NOTIFY_BY_EMAIL_FIELD;
        $scenarios['create'][]   = self::NOTIFY_BY_BROWSER_FIELD;

        $scenarios['update'][]   = self::NOTIFY_BY_EMAIL_FIELD;
        $scenarios['update'][]   = self::NOTIFY_BY_BROWSER_FIELD;

        return $scenarios;
    }

    public function rules()
    {
        $rules = parent::rules();

        $rules['notifyFieldsBool'] = [
            [
                self::NOTIFY_BY_BROWSER_FIELD,
                self::NOTIFY_BY_EMAIL_FIELD,
            ],
            'boolean',
        ];

        return $rules;
    }

    public function register()
    {
        $this->role_id = self::ROLE_ID_USER;

        return parent::register();
    }

    public function attributeLabels()
    {
        $labels = parent::attributeLabels();

        $labels[self::NOTIFY_BY_EMAIL_FIELD] = 'Enable e-mail notifications';
        $labels[self::NOTIFY_BY_BROWSER_FIELD] = 'Enable browser notifications';

        return $labels;
    }
}
