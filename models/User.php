<?php
namespace app\models;

use dektrium\user\models\User as BaseUser;

class User extends BaseUser
{
    const EVENT_PASSWORD_CHANGED = 'passwordChanged';

    const ROLE_ID_USER = 100;
    const ROLE_ID_MANAGER = 200;
    const ROLE_ID_ADMIN = 300;

    const ROLE_ID_FIELD = 'role_id';

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        // add field to scenarios
        $scenarios['create'][]   = self::ROLE_ID_FIELD;
        $scenarios['update'][]   = self::ROLE_ID_FIELD;
        $scenarios['register'][]   = self::ROLE_ID_FIELD;

        return $scenarios;
    }

    public function rules()
    {
        $rules = parent::rules();
        // add some rules
        $rules['roleIdRequired'] = [
            self::ROLE_ID_FIELD,
            'required',
        ];

        $rules['roleIdRange']   = [
            self::ROLE_ID_FIELD,
            'in',
            'range' => array_keys(self::getRolesIdsList()),
        ];

        return $rules;
    }

    public static function getRolesIdsList()
    {
        return [
            self::ROLE_ID_ADMIN => 'Admin',
            self::ROLE_ID_USER => 'User',
            self::ROLE_ID_MANAGER => 'Manager',
        ];
    }

    public function register()
    {
        $this->role_id = self::ROLE_ID_USER;

        return parent::register();
    }

    public function getRole()
    {
        return self::getRolesIdsList()[$this->role_id];
    }

    public function getIsAdmin()
    {
        if (parent::getIsAdmin()) {
            return true;
        }

        return $this->role_id == self::ROLE_ID_ADMIN;
    }

    public function getIsManager()
    {
        return $this->role_id == self::ROLE_ID_MANAGER;
    }
}
