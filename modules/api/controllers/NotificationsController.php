<?php
namespace app\modules\api\controllers;

class NotificationsController extends RestApplicationController
{

    public $modelClass = 'app\models\Notification';

    public function actions()
    {
        return [
            'index' => [
                'class' => 'app\modules\api\controllers\notifications\IndexAction',
                'modelClass' => $this->modelClass,
            ],
        ];
    }
}
