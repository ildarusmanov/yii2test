<?php
namespace app\modules\api\controllers;

use yii\rest\ActiveController;

class RestApplicationController extends ActiveController
{
    const TOKEN_PARAM = 'accessToken';
    const ACCESS_TOKEN = 'qwerty';

    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return false;
        }

        $this->requireAccessToken();

        return true;
    }

    protected function requireAccessToken()
    {
        $accessToken = \Yii::$app->request->getQueryParam(self::TOKEN_PARAM);

        if ($accessToken !== self::ACCESS_TOKEN) {
            throw new \yii\web\UnauthorizedHttpException('Invalid token');
        }
    }
}
