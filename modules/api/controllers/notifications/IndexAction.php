<?php
namespace app\modules\api\controllers\notifications;

use app\models\Notification;

class IndexAction extends \yii\rest\Action
{
    const DEFAULT_PAGE_SIZE = 100;

    public function run()
    {
        $models = Notification::find()
            ->limit(self::DEFAULT_PAGE_SIZE)
            ->indexBy('id')
            ->all();

        \Yii::$app->db->createCommand()->delete(
            Notification::tableName(),
            ['id' => array_keys($models)]
        )->execute();

        return $models;
    }
}
