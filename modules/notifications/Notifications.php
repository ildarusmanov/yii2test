<?php

namespace app\modules\notifications;

/**
 * Notifications module definition class
 */
class Notifications extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\notifications\controllers';
    public $layout = '@app/modules/notifications/views/layouts/main';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
