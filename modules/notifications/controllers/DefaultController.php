<?php
namespace app\modules\notifications\controllers;

use yii\web\Controller;

/**
 * Default controller for the `Notifications` module
 */
class DefaultController extends BaseController
{
    public function actionIndex()
    {
        return $this->render('index');
    }
}
