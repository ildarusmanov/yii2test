<?php
namespace app\modules\notifications\controllers;

use yii\filters\AccessControl;
use app\modules\notifications\models\NotificationForm;

class NotificationsController extends BaseController
{
    public function actionCreate()
    {
    	$request = \Yii::$app->request;
    	$model = new NotificationForm();

    	if ($request->isPost
    		&& $model->load($request->post())
		) {
    		$model->send();

            \Yii::$app->session->setFlash(
                'success',
                \Yii::t('app', 'Success')
            );

    		return $this->redirect(['/notifications']);
		}

        return $this->render('create', ['model' => $model]);
    }
}
