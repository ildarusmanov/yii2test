<?php
namespace app\modules\notifications\models;

use Yii;
use yii\base\Model;
use app\models\User;

class NotificationForm extends Model
{
    public $message;
    public $user_role_id;
    public $user_id;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['message'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
            // verifyCode needs to be entered correctly
            ['verifyCode', 'captcha'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'message' => 'Notification Message',
            'user_role_id' => 'Receivers User Group',
            'user_id' => 'Receiver User',
        ];
    }

    public function send()
    {
        $receivers = $this->getReceivers();

        \Yii::$app->notifications->sendCollection(
            $receivers,
            $this->message
        );

        return true;
    }

    protected function getUser()
    {
        return User::findOne($this->user_id);
    }

    protected function getRoleUsers()
    {
        $users = User::find()->where([
            'user_role_id' => $this->user_role_id,
        ])->indexBy('id')->all();

        return $users;
    }

    protected function getAllUsers()
    {
        return User::find()->all();
    }

    protected function getReceivers()
    {
        if (!empty($this->user_role_id)) {
            return $this->getRoleUsers();
        }

        if (!empty($this->user_id)) {
            return [$this->getUser()];
        }

        return $this->getAllUsers();
    }
}
