<?php
$this->title = Yii::t('app', 'Notifications Dashboard');
$this->params['breadcrumbs'][] = $this->title;

$this->params['pageControls'][] = [
    'label' => Yii::t('app', 'Send Custom Notification'),
    'url' => ['/notifications/notifications/create'],
    'options' => [
            'class' => 'btn btn-success',
        ],
];

?>
<div class="Notifications-default-index">
    <h1>Welcome!</h1>
</div>
