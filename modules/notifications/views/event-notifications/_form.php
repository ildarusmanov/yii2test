<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\helpers\FormHelper;

/* @var $this yii\web\View */
/* @var $model app\models\EventNotification */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="event-notification-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'eventid_id')->dropDownList(
	        FormHelper::getEventidsList(),
	        ['prompt' => 'Select event']
        ); ?>

    <?= $form->field($model, 'sender_id')->dropDownList(
	        FormHelper::getSendersList(),
	        ['prompt' => 'Select sender provider']
        ); ?>

    <?= $form->field($model, 'user_role_id')->dropDownList(
	        FormHelper::getUserGroupsList(),
	        ['prompt' => 'Select receiver group']
        ); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
