<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Event Notifications');
$this->params['breadcrumbs'][] = $this->title;

$this->params['pageControls'][] = [
    'label' => Yii::t('app', 'Create Event Notification'),
    'url' => ['create'],
    'options' => [
            'class' => 'btn btn-success',
        ],
];

?>
<div class="event-notification-index">

<h1><?= Html::encode($this->title) ?></h1>

<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'eventid_id',
            'sender_id',
            'user_role_id',
            'content:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
