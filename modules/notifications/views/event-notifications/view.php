<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\EventNotification */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Event Notifications'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['pageControls'][] = [
        'label' => Yii::t('app', 'Update'),
        'url' => ['update', 'id' => $model->id],
        'options' => [
            'class' => 'btn btn-primary',
        ],

    ];

$this->params['pageControls'][] = [
        'label' => Yii::t('app', 'Delete'),
        'url' => ['delete', 'id' => $model->id],
        'options' => [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ],
    ];
?>
<div class="event-notification-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'eventid_id',
            'sender_id',
            'user_role_id',
            'content:ntext',
        ],
    ]) ?>

</div>
