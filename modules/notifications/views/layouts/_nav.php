<?php
use yii\helpers\Html;

$current = \Yii::$app->controller->id;

$nav = [
	'default' => [
		'Dashboard',
		'/notifications',
	],
	'event-notifications' => [
		'Notifications',
		'/notifications/event-notifications',
	],

	'eventids' => [
		'Events',
		'/notifications/eventids',
	],

	'senders' => [
		'Providers',
		'/notifications/senders',
	],
];
?>

<ul class="nav nav-pills nav-stacked">
	<?php foreach ($nav as $key => $item): ?>
		<li <?= $key == $current ? 'class="active"' : '' ?>><?= Html::a($item[0], $item[1]) ?></li>
	<?php endforeach; ?>
</ul>
