<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\widgets\pagecontrolswidget\PageControlsWidget;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= \Yii::$app->language ?>">
<head>
    <meta charset="<?= \Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body data-user-id="<?= \Yii::$app->user->id ?>">
<?php $this->beginBody() ?>

<div class="wrap">
    <?= $this->render('@app/views/shared/_navbar') ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>

        <?php
            if (isset($this->params['pageControls'])
                && sizeof($this->params['pageControls']) > 0
            ):
        ?>
            <div class="page-controls">
                <?= PageControlsWidget::widget([
                    'items' => $this->params['pageControls'],
                ]); ?>
            </div>
        <?php endif; ?>

        <div class="row">
            <div class="col-md-3">
                <?= $this->render('_nav') ?>
            </div>
            <div class="col-md-9">
                <?= $content ?>
            </div>
        </div>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Wajox <?= date('Y') ?></p>

        <p class="pull-right"><?= \Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
