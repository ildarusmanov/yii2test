<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\helpers\FormHelper;

?>

<div class="notification-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_role_id')->dropDownList(
	        FormHelper::getUserGroupsList(),
	        ['prompt' => 'Select receiver group']
        ); ?>

    <?= $form->field($model, 'user_id')->dropDownList(
	        FormHelper::getUsersList(),
	        ['prompt' => 'Select receiver user']
        ); ?>

    <?= $form->field($model, 'message')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Send Notification'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
