<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Eventid */

$this->title = Yii::t('app', 'Send Notifications');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notifications-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
