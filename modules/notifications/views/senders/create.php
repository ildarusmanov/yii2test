<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\NotificationSender */

$this->title = Yii::t('app', 'Create Notification Sender');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Notification Senders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notification-sender-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
