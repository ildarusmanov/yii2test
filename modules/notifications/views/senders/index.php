<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Notification Senders');
$this->params['breadcrumbs'][] = $this->title;

$this->params['pageControls'][] = [
    'label' => Yii::t('app', 'Add Sender Provider'),
    'url' => ['create'],
    'options' => [
            'class' => 'btn btn-success',
        ],
];
?>
<div class="notification-sender-index">

    <h1><?= Html::encode($this->title) ?></h1>

<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'sender_class',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
