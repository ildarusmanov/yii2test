<?php
return [
    'manageNews' => [
        'type' => 2,
        'description' => 'Create, Update, Delete news',
    ],
    'manageNotifications' => [
        'type' => 2,
        'description' => 'Create, Update, Delete Templates, Send Notifications',
    ],
    'viewNews' => [
        'type' => 2,
        'description' => 'View full news content',
    ],
    'manageUsers' => [
        'type' => 2,
        'description' => 'View, Create, Update, Delete users',
    ],
    'user' => [
        'type' => 1,
        'description' => 'User',
        'ruleName' => 'userRole',
        'children' => [
            'viewNews',
        ],
    ],
    'manager' => [
        'type' => 1,
        'description' => 'Manager/Moderator',
        'ruleName' => 'userRole',
        'children' => [
            'manageNews',
            'user',
        ],
    ],
    'admin' => [
        'type' => 1,
        'description' => 'Administrator',
        'ruleName' => 'userRole',
        'children' => [
            'manageUsers',
            'manageNotifications',
            'manager',
        ],
    ],
];
