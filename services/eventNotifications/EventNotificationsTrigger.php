<?php
namespace app\services\eventNotifications;

use app\models\EventNotification;
use app\models\Eventid;

class EventNotificationsTrigger
{
    protected $eventClass;
    protected $eventId;
    protected $event;

    public function __construct($eventClass, $eventId, $event)
    {
        $this->setEventClass($eventClass)
             ->setEventId($eventId)
             ->setEvent($event);
    }

    public function trigger()
    {
        foreach ($this->getEventNotifications() as $eventNotification) {
            $this->processEventNotification($eventNotification);
        }
    }

    protected function processEventNotification($eventNotification)
    {
        $params = $this->getNotificationParams();
        $receivers = $eventNotification->subscribedUsers;
        $content = $eventNotification->content;
        $senderIds = [$eventNotification->sender_id];
        
        if (sizeof($receivers) == 0) {
            return;
        }

        \Yii::$app->notifications->sendCollection(
            $receivers,
            $content,
            $params,
            $senderIds
        );
    }

    protected function setEventClass($eventClass)
    {
        $this->eventClass = $eventClass;

        return $this;
    }

    protected function getEventClass()
    {
        return $this->eventClass;
    }

    protected function setEventId($eventId)
    {
        $this->eventId = $eventId;

        return $this;
    }

    protected function getEventId()
    {
        return $this->eventId;
    }

    protected function setEvent($event)
    {
        $this->event = $event;

        return $this;
    }

    protected function getEvent()
    {
        return $this->event;
    }

    protected function getEventNotifications()
    {
        $eventids = Eventid::find()->where([
            'event_class' => $this->getEventClass(),
            'event_id' => $this->getEventId(),
        ])->indexBy('id')
          ->all();

        return EventNotification::find()->where([
               'eventid_id' => array_keys($eventids),
            ])->all();
    }

    protected function getNotificationParams()
    {
        return [
            'object' => $this->getEvent()->sender,
            'data' => $this->getEvent()->data,
        ];
    }
}
