<?php
namespace app\services\events;

use yii\base\Event;
use yii\base\Object;
use app\services\eventNotifications\EventNotificationsTrigger;
use app\models\Eventid;

class EventsProcessor extends Object
{
    public function __construct($params = [])
    {
        if (isset($params['handlers'])) {
            $this->addHandlers($params['handlers']);
        }

        $this->bindCustomTriggers();
    }

    protected function addHandlers($handlers)
    {

        $this->trace(__METHOD__, func_get_args());

        foreach ($handlers as $className) {
            (new $className())->bindEvents($this);
        }

        return $this;
    }

    public function bindTrigger($className, $typeId)
    {
        $this->trace(__METHOD__, func_get_args());

        $this->on($className, $typeId, function ($event) use ($className, $typeId) {
            \Yii::trace('Event called!!!' . $className . $typeId);
            \Yii::$app->events->handleCustomTriggers($className, $typeId, $event);
        });

        return $this;
    }

    public function on($className, $typeId, $callback)
    {
        $this->trace(__METHOD__, func_get_args());

        Event::on($className, $typeId, $callback);

        return $this;
    }

    public function trigger($className, $typeId, $event = null)
    {
        $this->trace(__METHOD__, func_get_args());

        Event::trigger($className, $typeId, $event = null);

        return $this;
    }

    protected function bindCustomTriggers()
    {
        foreach (Eventid::find()->each() as $event) {
            $this->bindTrigger($event->event_class, $event->event_id);
        }

    }

    protected function handleCustomTriggers($className, $typeId, $event = null)
    {
        $this->trace(__METHOD__, func_get_args());

        (new EventNotificationsTrigger($className, $typeId, $event))->trigger();
    }

    protected function trace($func, $params)
    {
        $message = 'Class ' . __CLASS__ . ' '
            . $func .'(' . json_encode($params) . ')';

        \Yii::info($message);
    }
}
