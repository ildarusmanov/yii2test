<?php
namespace app\services\events\handlers;

abstract class HandlerAbstract
{
	abstract public function bindEvents($eventsManager);
}