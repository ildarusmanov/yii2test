<?php
namespace app\services\events\handlers;

use app\models\User;

class UserEventsHandler extends HandlerAbstract
{
	public function bindEvents($eventsManager)
	{
		$eventsManager->on(
			User::className(),
			User::EVENT_AFTER_UPDATE,
			function($e) {
				if (!isset($e->changedAttributes['password_hash'])) {
					return;
				}

				$message = 'Your password has been changed';

				\Yii::$app->notifications->sendOne($e->sender, $message);
			}
		);
	}
}