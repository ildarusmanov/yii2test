<?php
namespace app\services\news;

use app\models\News;

class NewsBuilder
{
    protected $user;
    protected $model;

    public function __construct($user, $model = null)
    {
        $this->setUser($user)
             ->setModel($model);
    }

    public function getModel()
    {
        return $this->model;
    }

    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }

    public function save($request)
    {
        try {
            $this->buildModel()
                 ->loadRequest($request)
                 ->validateModel()
                 ->saveModel();
        } catch (\Exception $e) {
            \Yii::trace($e->getMessage());

            return false;
        }

        return true;
    }

    protected function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    protected function getUser()
    {
        return $this->user;
    }

    protected function buildModel()
    {
        $model = $this->getModel();

        if ($model == null) {
            $model = new News();
        }

        if ($model->isNewRecord) {
            $model->created_at = time();
            $model->user_id = $this->getUser()->id;
        }

        $this->setModel($model);

        return $this;
    }

    protected function loadRequest($request)
    {
        if (!$this->getModel()->load($request->post())) {
            throw new \Exception('Can not load data from request!');
        }

        return $this;
    }

    protected function validateModel()
    {
        if (!$this->getModel()->validate()) {
            throw new \Exception('News data is invalid!');
        }

        return $this;
    }

    protected function saveModel()
    {
        if (!$this->getModel()->save()) {
            throw new \Exception('Can not save this news');
        }

        return $this;
    }
}
