<?php
namespace app\services\notifications;

class NotificationsBuilder
{
    protected $user;
    protected $senders = [];

    public function __construct($user, $senders = [])
    {
        $this->setUser($user)
             ->setSenders($senders);
    }

    public function send($message, $params = [])
    {
        $message = $this->prepareMessage($message, $params);

        foreach ($this->getSenders() as $id => $senderModel) {
            $senderClass = $senderModel->sender_class;
            $sender = new $senderClass($this->getUser());

            if (!$sender->canSend()) {
                continue;
            }

            $sender->send($message);
        }
    }

    protected function setSenders($senders)
    {
        $this->senders = $senders;

        return $this;
    }

    protected function getSenders()
    {
        return $this->senders;
    }

    protected function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    protected function getUser()
    {
        return $this->user;
    }

    protected function prepareMessage($message, $params = [])
    {
        if (sizeof($params) == 0) {
            return $message;
        }

        foreach ($params as $key => $value) {
            $message = str_replace(
                    '[[' . $key . ']]',
                    $value,
                    $message
                );
        }

        return $message;
    }
}
