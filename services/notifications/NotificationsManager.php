<?php
namespace app\services\notifications;

use yii\base\Component;
use app\models\NotificationSender;

class NotificationsManager extends Component
{
    protected $senders;

    public function __construct()
    {
        $this->loadSenders();
    }

    public function sendOne($user, $message, $params = [], $onlySendersIds = [])
    {
        $senders = $this->getSenders($onlySendersIds);
        $message = $this->buildMessageText($user, $message, $params);
        (new NotificationsBuilder($user, $senders))->send($message);
    }

    public function sendCollection($users, $message, $params = [], $onlySendersIds = [])
    {
        \Yii::trace('Trying send the collection$$$');

        foreach ($users as $user) {
            $this->sendOne($user, $message, $params, $onlySendersIds);
        }
    }

    public function getSenders($only = [])
    {
        if (sizeof($only) > 0) {
            $result = [];
            foreach ($only as $id) {
                $result[$id] = $this->senders[$id];
            }
            return $result;
        }
        return $this->senders;
    }

    protected function buildMessageText($user, $message, $params)
    {
        $params['user'] = $user;

        return $this->render($message, $params);
    }

    protected function render($_template_, $_params_)
    {
        ob_start();
        ob_implicit_flush(false);
        extract($_params_, EXTR_OVERWRITE);
        eval('?>' . $_template_);

        return ob_get_clean();
    }

    protected function loadSenders()
    {
        $senders = NotificationSender::find()->indexBy('id')->all();

        $this->setSenders($senders);

        return $this;
    }

    protected function setSenders($senders)
    {
        $this->senders = $senders;

        return $this;
    }
}
