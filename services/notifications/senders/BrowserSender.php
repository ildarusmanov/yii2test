<?php
namespace app\services\notifications\senders;

use app\models\Notification;

class BrowserSender extends SenderAbstract
{
	public function canSend()
	{
		return ($this->getUser()->profile->notify_by_browser  == 1);
	}

	public function send($message)
	{
		return $this->addToQueue($message);
	}

	protected function addToQueue($message)
	{
		$model = new Notification();
		$model->user_id = $this->getUser()->id;
		$model->message = $message;

		return $model->save();
	}
}