<?php
namespace app\services\notifications\senders;

class EmailSender extends SenderAbstract
{
	public function canSend()
	{
		return ($this->getUser()->profile->notify_by_email  == 1);
	}

	public function send($message)
	{
		return $this->composeEmail($message);
	}

	protected function composeEmail($message)
	{
		\Yii::$app->mailer->compose()
			->setHtmlBody($message)
		    ->setTo($this->getUser()->email)
		    ->setSubject('New notification')
		    ->send();
	}
}