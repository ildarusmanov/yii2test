<?php
namespace app\services\notifications\senders;

abstract class SenderAbstract
{
	protected $user;

	public function __construct($user)
	{
		$this->setUser($user);
	}

	protected function setUser($user)
	{
		$this->user = $user;

		return $this;
	}

	protected function getUser()
	{
		return $this->user;
	}
	
	abstract function canSend();

	abstract function send($message);
}