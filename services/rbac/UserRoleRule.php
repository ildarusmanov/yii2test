<?php
namespace app\services\rbac;

use Yii;
use yii\rbac\Rule;
use yii\helpers\ArrayHelper;
use app\models\User;

class UserRoleRule extends Rule
{
    public $name = 'userRole';

    public function execute($user, $item, $params)
    {
        if (\Yii::$app->user->isGuest) {
            return false;
        }

        $user = \Yii::$app->user->identity;

        if ($user == null) {
            return false;
        }

        $user = ArrayHelper::getValue($params, 'user', $user);

        if ($item->name === 'admin') {
            return $user->isAdmin;
        } elseif ($item->name === 'manager') {
            return $user->isAdmin || $user->isManager;
        } elseif ($item->name === 'user') {
            return true;
        }

        return false;
    }
}
