<?php
use yii\helpers\Html;
?>
<div class="news-item">
	<h2><?= $model->title ?></h2>
	<p class="text-muted"><?= $model->createdDate ?></p>
	<p><?= $model->shortDescription ?></p>
	<?php if (\Yii::$app->user->can('viewNews')): ?>
		<p><?= Html::a('Read More...', ['view', 'id' => $model->id]) ?></p>
	<?php else: ?>
		<p class="text-muted">Sign up to obtain full view access</p>
	<?php endif; ?>
</div>
