<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\widgets\ListView;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'News');
$this->params['breadcrumbs'][] = $this->title;

if (\Yii::$app->user->can('manageNews')) {
    $this->params['pageControls'][] = [
        'label' => Yii::t('app', 'Create News'),
        'url' => ['create'],
        'options' => [
                'class' => 'btn btn-success',
            ],
    ];
}

?>
<div class="news-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<?php Pjax::begin(); ?>
		<p>
			<ul class="list list-inline">
				<li>News per page:</li>
				<?php foreach ([100, 50, 20] as $limitSize): ?>
					<li <?= $limit == $limitSize ? 'class="active"' : ''?>>
						<?= Html::a($limitSize, ['news/index', 'limit' => $limitSize]) ?>
					</li>
				<?php endforeach; ?>
			</ul>
	    </p>

	    <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_item',
        ]); ?>
	<?php Pjax::end(); ?>
</div>
