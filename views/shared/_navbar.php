<?php
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

NavBar::begin([
    'brandLabel' => 'NewsBox',
    'brandUrl' => \Yii::$app->homeUrl,
    'options' => [
        'class' => 'navbar-inverse navbar-fixed-top',
    ],
]);
$navItems = [
        ['label' => 'News', 'url' => ['/']],
];

if (\Yii::$app->user->can('manageUsers')) {
    $navItems[] =  ['label' => 'User', 'url' => ['/user/admin']];
}

if (\Yii::$app->user->can('manageNotifications')) {
    $navItems[] =  ['label' => 'Notifications', 'url' => ['/notifications']];
}

if (\Yii::$app->user->isGuest) {
    $navItems[] = ['label' => 'Login', 'url' => ['/user/security/login']];
} else {
    $navItems[] = ['label' => 'Account Settings', 'url' => ['/user/settings/account']];
    $navItems[] = [
        'label' => 'Logout (' . \Yii::$app->user->identity->email . ')',
        'url' => ['/user/security/logout'],
        'linkOptions' => ['data-method' => 'post'],
    ];
}

echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right'],
    'items' => $navItems,
]);

NavBar::end();