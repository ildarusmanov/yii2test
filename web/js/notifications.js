var Notifications = function(socketAddr) {
	this.socketAddr = socketAddr;
	this.sokect = null;
	this.online = false;

	this.connect = function() {
		this.socket = new WebSocket(socketAddr);
	};

	this.setOnline = function()
	{
		this.online = true;
		alertify.log('Connected to notifications server!');
	}

	this.setOffline = function()
	{
		this.online = false;
	}

	this.parseMessage = function(data)
	{
		alertify.log(data);
	}

	this.bindEvents = function () {
		var self = this;

		this.socket.onmessage = function(event) {
			self.parseMessage(event.data);
		};

		this.onopen = function() {
			self.setOnline();
		}

		this.onclose = function() {
			self.setOffline();
		}
	};

	this.connect(socketAddr);
	this.bindEvents();
};

$(document).ready(function(e){
	var userId = parseInt($('body').attr('data-user-id'));

	if (userId == 0) {
		return;
	}

	new Notifications('ws://0.0.0.0:3000?userId=' + userId);	
});
