<?php

namespace app\widgets\pagecontrolswidget;

use yii\base\Widget;

class PageControlsWidget extends Widget
{
    public $items = [];

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->render('page_controls_widget', [
            'items' => $this->items,
        ]);
    }
}
