<?php
use yii\helpers\Html;
?>
<?php if (sizeof($items) > 0): ?>
	<div class="btn-group page-controls" role="group">
		<?php

		foreach ($items as $item) {
		    echo Html::a($item['label'], $item['url'], isset($item['options']) ? $item['options'] : []);
		}

		?>
	</div>
<?php endif; ?>
