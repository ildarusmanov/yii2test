var WebSocketServer = new require('ws'),
    request = new require('request'),
    url = require('url'),
    queue = {},
    usersList = {},
    wsList = {},
    lastConn = 0,
    apiUrl = 'http://0.0.0.0:5000/api/notifications',
    apiAccessToken = 'qwerty',
    interval = 1000;

var intervalId = setInterval(function(){
  request(apiUrl + '?accessToken=' + apiAccessToken, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      var jsonList = JSON.parse(body);
      for (i in jsonList) {
        var notification = jsonList[i];

        sendNotification(notification);
      }
    }

    console.log('api request');
  });
}, interval);

var webSocketServer = new WebSocketServer.Server({
  port: 3000
});

webSocketServer.on('connection', function(ws) {
  var location = url.parse(ws.upgradeReq.url, true);
  var userId = location.query.userId;
  var wsId = lastConn;

  lastConn++;

  console.log('Connection open');
  console.log(location);

  addUserWs(ws, wsId, userId);

  ws.on('close', function() {
    console.log('Connection closed');
    deleteConnection(wsId);
  });
});

var addUserWs = function(ws, wsId, userId) {
  var currentUser = usersList[userId];
  if (typeof currentUser == 'undefined') {
    currentUser = {
      wsIds: [wsId],
    }
  } else {
    currentUser.wsIds.push(wsId);
  }

  wsList[wsId] = {
    userId: userId,
    ws: ws,
  };

  usersList[userId] = currentUser;

  sendFromQueue(userId);
};

var deleteConnection = function(wsId) {
  var currentWs = wsList[wsId];

  if (typeof currentWs == 'undefined') {
    return;
  }

  var userId = currentWs.userId;
  var currentUser = usersList[userId];
  var wsIdIndex = currentUser.wsIds.indexOf(wsId);

  delete currentUser.wsIds[wsIdIndex];

  if (currentUser.wsIds.length == 0) {
    delete usersList[userId];
  }

  delete wsList[wsId];
};

var addToQueue = function(notification) {
  var userQueue = queue[notification.user_id];

  if (typeof userQueue == 'undefined') {
    var userQueue = [notification];
  } else {
    userQueue.push(notification);
  }

  queue[notification.user_id] = userQueue;
};

var sendFromQueue = function(userId) {
  var userQueue = queue[userId];

  if (typeof userQueue == 'undefined') {
    return;
  }

  for (i in userQueue) {
    sendNotification(userQueue[i]);
  }

  delete queue[userId];
}

var sendNotification = function(notification) {
  var userId = notification.user_id;
  var message = notification.message;

  var currentUser = usersList[userId];
  if (typeof currentUser == 'undefined') {
    addToQueue(notification);
    return;
  }

  for (i in currentUser.wsIds) {
    var userWsId = currentUser.wsIds[i];
    wsList[userWsId].ws.send(message);
  }
};
